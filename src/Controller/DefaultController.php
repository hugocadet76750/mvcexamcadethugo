<?php

namespace App\Controller;

use Core\Kernel\AbstractController;
use App\Model\ContactModel;
use App\Service\Form;
use App\Service\Validation;

/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {
        $message = 'Examen CADET Hugo MVC';
        //$this->dump($message);
        $this->render('app.default.frontpage',array(
            'message' => $message,
        ));
    }

    public function contact()
    {
        $textButton = 'Envoyer';
        $message = 'Page Contact';

        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors))  {
                ContactModel::insert($post);
//                $this->redirect('contact');
            }
        }
        $form = new Form($errors);

        $this->render('app.default.contact',array(
            'message' => $message,
            'textButton' => $textButton,
            'form' => $form,
        ));
    }

    public function listing() {
        $message = 'Page Listing Contacts';
        $this->render('app.default.listing', array(
            'contacts' => ContactModel::allBy('id','DESC'),
            'message' => $message,
        ));
    }

    private function validate($v,$post)
    {
        $errors = [];
        $errors['sujet'] = $v->textValid($post['sujet'], 'sujet',2, 150);
        $errors['email'] = $v->textValid($post['email'], 'email',5, 150);
        $errors['message'] = $v->textValid($post['message'], 'message',5, 3000);
        return $errors;
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
