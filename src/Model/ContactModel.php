<?php

namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;


class ContactModel extends AbstractModel
{
    protected static $table = 'contact';
    protected int $id;
    protected string $sujet;
    protected string $email;
    protected string $message;
    protected $createdAt;

    public static function allBy($type,$order)
    {
        return App::getDatabase()->query("SELECT * FROM ".self::getTable(). " ORDER BY $type $order" ,get_called_class());
    }

    public static function insert($post){
        App::getDatabase()->prepareInsert("INSERT INTO " . self::getTable() . " (sujet,email,message,created_at) VALUES (?,?,?,NOW())",array($post['sujet'],$post['email'],$post['message']));
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSujet(): string
    {
        return $this->sujet;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }


}