<h1 style="text-align: center;font-size:33px;margin: 100px 0;color:#A67153;">
    <?= $message; ?>
</h1>

<table id="contact" class="wraptable">
    <thead class="thead">
        <tr class="allth">
            <th>ID</th>
            <th>Sujet</th>
            <th>Mail</th>
            <th>Message</th>
            <th>Date</th>
            <th>Time</th>
        </tr>
    </thead>


    <?php
    echo '<tbody>';
    foreach ($contacts as $contact) {
        echo '<tr>';
            echo '<td>'.$contact->id.'</td>';
            echo '<td>'.$contact->sujet.'</td>';
            echo '<td>'.$contact->email.'</td>';
            echo '<td>'.$contact->message.'</td>';
            echo '<td>'.date('d/m/Y', strtotime($contact->created_at)).'</td>';
            echo '<td>'.date('h:i a', strtotime($contact->created_at)).'</td>';
        echo '</tr>';
    }
    echo '</tbody>';
    ?>


</table>