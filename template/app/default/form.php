<form action="" method="post" novalidate class="wrapform">
    <?php echo $form->label('Sujet'); ?>
    <?php echo $form->input('sujet') ?>
    <?php echo $form->error('sujet'); ?>
    <?php echo '<br>'; ?>
    <?php echo $form->label('Email'); ?>
    <?php echo $form->input('email','email'); ?>
    <?php echo $form->error('email'); ?>
    <?php echo '<br>'; ?>
    <?php echo $form->label('Message'); ?>
    <?php echo $form->textarea('message'); ?>
    <?php echo $form->error('message'); ?>
    <?php echo '<br>'; ?>
    <?php echo $form->submit('submitted', $textButton); ?>
</form>